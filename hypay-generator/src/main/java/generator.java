import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import com.baomidou.mybatisplus.generator.fill.Column;
import com.baomidou.mybatisplus.generator.fill.Property;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class generator {
    public static void main(String[] args) {
        //输入作者名
        String authorName = "王富贵";

        //输入数据库名
        String databaseName = "hypay";

        //输入输出路径,如果没有，请写 String outUrl = "/";
        String outUrl = "/hypay-generator";

        //输入组
        String group = "com.hy";

        //输入表前缀名
        String tablePrefix = "pay_";

        //输入表名，用逗号隔开
        String list = "pay_order";
        List<String> tables = Arrays.stream(list.split(",")).collect(Collectors.toList());

        //配置
        FastAutoGenerator.create(
                        "jdbc:mysql://localhost:3306/" + databaseName + "?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8",
                        "root",
                        "123456")
                .globalConfig(builder -> {
                    builder.author(authorName)//作者
                            .outputDir(System.getProperty("user.dir") + outUrl + "/src/main/java")//输出路径(写到java目录)
                            .enableSwagger()//开启swagger
                            .disableOpenDir()
                            .commentDate("yyyy-MM-dd")
                            .fileOverride();//开启覆盖之前生成的文件

                })
                .packageConfig(builder -> {
                    builder.parent(group)
                            .moduleName("") //模块名，加在com.hy后面
                            .entity("entity")
                            .service("service")
                            .serviceImpl("service.Impl")
                            .controller("controller")
                            .mapper("mapper")
                            .xml("mapper")//mapperXML名
                            //mapperxml输出路径定制
                            .pathInfo(Collections.singletonMap(OutputFile.mapperXml, System.getProperty("user.dir") + outUrl + "/src/main/resources/mapper"));
                })
                .strategyConfig(builder -> {
                    builder.addInclude(tables)
                            .addTablePrefix(tablePrefix)//表前缀,会把生成的类的类名前缀去掉

                            .entityBuilder()
                            .enableLombok()//lombok生成
                            .enableTableFieldAnnotation()//启用字段注释
                            .logicDeleteColumnName("deleted")//逻辑删除字段名（数据库）
                            .addTableFills(new Column("created_time", FieldFill.INSERT))//删除时间
                            .addTableFills(new Column("updated_time", FieldFill.INSERT_UPDATE))//修改时间
                            .addTableFills(new Property("createdTime",FieldFill.INSERT))
                            .addTableFills(new Property("updatedTime",FieldFill.INSERT_UPDATE))

                            .mapperBuilder()
                            .superClass(BaseMapper.class)
                            .enableBaseResultMap()//基础map映射
                            .enableBaseColumnList()//基础行列表映射
                            .formatMapperFileName("%sMapper")
                            .enableMapperAnnotation()
                            .formatXmlFileName("%sMapper")

                            .serviceBuilder()
                            .formatServiceFileName("%sService")
                            .formatServiceImplFileName("%sServiceImpl")

                            .controllerBuilder()
                            .formatFileName("%sController")
                            .enableRestStyle();
                })
                .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .execute();
    }
}
