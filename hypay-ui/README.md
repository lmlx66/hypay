## 🍕启动项目

``` 
# 安装依赖
npm install

# 启动项目
npm run serve

# 项目打包
npm run build
```



## 🍔项目截图

支付页面

![3](https://cdn.jsdelivr.net/gh/lmlx6688/img1/hypay/3.png)



订单页面

![4](https://cdn.jsdelivr.net/gh/lmlx6688/img1/hypay/4.png)



动态接口文档页面

![5](https://cdn.jsdelivr.net/gh/lmlx6688/img1/hypay/5.png)

