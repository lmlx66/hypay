import Vue from 'vue'
import App from './App.vue'
import router from './router'
import { 
  Button,
  Select,
  Card,
  Radio,
  RadioGroup,
  RadioButton,
  Row,
  Col,
  Dialog,
  Input,
  MessageBox,
  Table,
  TableColumn,
} from 'element-ui';
Vue.use(Button)
Vue.use(Select)
Vue.use(Card)
Vue.use(Radio)
Vue.use(RadioGroup)
Vue.use(RadioButton)
Vue.use(Row)
Vue.use(Col)
Vue.use(Dialog)
Vue.use(Input)
Vue.use(Table)
Vue.use(TableColumn)
Vue.prototype.$confirm = MessageBox.confirm
Vue.config.productionTip = false
import store from './store'
import './style/reset.css'
new Vue({
  render: h => h(App),
  store,
  router
}).$mount('#app')
