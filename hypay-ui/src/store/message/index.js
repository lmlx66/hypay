//login模块的小仓库
// import {reqCategoryList} from '../../api/index'
import {reqList} from '../../api/index'
// import {reqskuId} from '../../api/index'
import {reqId} from '../../api/index'
const state={
    id:'',
    qr_code:''
}
const mutations={
    GETID(state,value){
        // console.log(value);
        state.id=value
    },
    GETLIST(state,value){
        state.qr_code=value.qr_code
    }
}
const actions={
   async getID({commit}){
        // let result= await reqCategoryList();
        let result= await reqId();
        // let result3= await reqskuId(value);
        // let result2= await reqList(params);
        // console.log(result);
        
        if(result.code=='200'){
            commit("GETID",result.data.id)
        }
        // console.log(result3);
      
    },
    async getList({commit},params){
        let result=await reqList(params)
        console.log(result);
        if(result.code==200){
            commit("GETLIST",result.data)
        }
    }
}
const getters={}
export default{
    state,
    mutations,
    actions,
    getters
}