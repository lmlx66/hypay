import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
import message from './message'
import zhifubao from './zhifubao'
import table from './table'
export default new Vuex.Store({
    modules:{
        message,
        zhifubao,
        table
    }
})