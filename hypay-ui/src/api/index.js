import requests from "./ajax";
// /api/product/getBaseCategoryList
// export const reqCategoryList=()=>{
//     return requests({url:'/product/getBaseCategoryList',method:'get'})
// }
// export const reqList=(params)=>{
//     return requests({url:'/list',method:'post',data:params})
// }
// export const reqskuId=(skuId)=>{
//     return requests({url:`/item/${ skuId }`,method:'get'})
// }
export const reqId=()=>{
    return requests({url:'/GetId',method:'get'})
}
export const reqList=(params)=>{
    return requests({url:'/aliPay/tradePreCreatePay',method:'post',data:params})
}
export const reqpcPay=(params)=>{
    return requests({url:'/aliPay/pcPay',method:'post',data:params})
}
export const reqTable=(params)=>{
    return requests({url:'/order/getPageOrderList',method:'post',data:params})
}