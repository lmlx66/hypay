import Vue from 'vue'
import VueRouter from 'vue-router'
import Pcpay from '../views/Pcpay.vue'
import Home from '../views/Home.vue'
Vue.use(VueRouter)
const routes=[
    {
        path:'/',
        redirect:'/home',
    },
    {
        path:'/home',
        component:Home,
        name:'home'
    },
    {
        path:'/pcpay',
        component:Pcpay,
        name:'pcpay'
    },
   
]
const router=new VueRouter({
    routes
})
export default router 