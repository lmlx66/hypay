package com.hy.common.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @author: 王富贵
 * @description: 订单vo实体
 * @createTime: 2022年04月21日 18:53:07
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "支付订单基础vo对象", description = "支付订单基础vo对象")
public class OrderVo implements Serializable {
    @ApiModelProperty("商户订单号")
    private Long outTradeNo;

    @ApiModelProperty("支付金额")
    private String amount;

    @ApiModelProperty("订单支付状态：（ 0-订单生成, 1-支付中, 2-支付成功, 3-支付失败, 4-已撤销, 5-已退款, 6-订单关闭）")
    private String state;

    @ApiModelProperty("支付方式（微信，支付宝）")
    private String payMethod;

    @ApiModelProperty("创建时间")
    private String createdTime;
}
