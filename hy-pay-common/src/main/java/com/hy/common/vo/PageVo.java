package com.hy.common.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "分页基础vo对象", description = "分页基础vo对象")
public class PageVo implements Serializable {
    @ApiModelProperty("页码数")
    private Integer current;

    @ApiModelProperty("每页数量")
    private Integer size;
}
