package com.hy.common.Result;

import lombok.Getter;

/**
 * @作用 自定义响应枚举
 * @作者 坏银
 * @时间 2022/4/17 12:13
 */
@Getter
public enum ResponseEnum {
    SERVER_ERROR(500, "服务器错误"),

    //订单相关问题
    ORDER_STATUS_IS_INVALID(600, "当前订单状态不合法"),
    ORDER_IS_NOT_IN_PAYMENT_STATUS(601, "已存在订单并且当前订单不是支付状态"),
    PARAMETER_IS_EMPTY(602, "请求参数为空"),

    //阿里支付相关错误
    A_LI_FAILED_TO_CREATE_ORDER(1001, "预创建订单失败"),
    A_LI_STREAM_ERROR(1002, "流创建失败"),
    A_LI_CONTROLLER_NEEDS_FOUNDATION(1003, "控制器需要基础抽象类"),
    A_LI_REPEAT_ORDER(1004, "请求失败，存在重复的订单号"),
    A_LI_ORDER_DOES_NOT_EXIST(1005, "订单不存在"),
    A_LI_FAILED_TO_REQUEST_ALIPAY_BACKEND_INTERFACE(1006, "请求支付宝后端接口失败，请重试"),
    A_LI_REQUEST_PARAMETER_IS_EMPTY_OR_DOES_NOT_EXIST(1007, "请求参数为空或不存在"),
    ;

    //错误码
    private final Integer code;
    //返回消息
    private final String message;

    ResponseEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
