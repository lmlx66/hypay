package com.hy.common.Result;

import lombok.Builder;
import lombok.Getter;

/**
 * @作用 统一结果返回
 * @作者 坏银
 * @时间 2022/4/17 12:13
 */
@Builder
@Getter
public class Result {
    //状态码
    private Integer code;
    //消息码
    private String message;
    //返回数据
    private Object data;

    public Result(Integer code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    /**
     * 有数据成功返回
     *
     * @param data 返回参数
     * @return R
     */
    public static Result success(Object data) {
        return new Result(ResultCodeEnum.SUCCESS.getCode(), ResultCodeEnum.SUCCESS.getMessage(), data);
    }

    /**
     * 无数据成功返回
     *
     * @return R
     */
    public static Result success() {
        return success("");
    }

    /**
     * 错误返回
     *
     * @param code    状态码
     * @param message 返回信息
     * @return R
     */
    public static Result error(Integer code, String message) {
        return new Result(code, message, null);
    }
}