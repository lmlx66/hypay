package com.hy.common.Result;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @作用 自定义结果枚举
 * @作者 坏银
 * @时间 2022/4/17 12:13
 */
@Getter
@AllArgsConstructor
public enum ResultCodeEnum {
    //成功就一种返回
    SUCCESS(200, "成功"),

    //错误
    ;

    //错误码
    private final Integer code;

    //返回消息
    private final String message;
}