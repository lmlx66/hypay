package com.hy.common.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

/**
 * 支付订单基础对象
 *
 * @author 王富贵
 * @since 2022-04-19
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("pay_order")
@ApiModel(value = "支付订单基础对象", description = "支付订单基础对象")
public class Order implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("商户订单号")
    @TableField("out_trade_no")
    private Long outTradeNo;

    @ApiModelProperty("支付金额")
    @TableField("amount")
    private String amount;

    @ApiModelProperty("订单支付状态：（ 0-订单生成, 1-支付中, 2-支付成功, 3-支付失败, 4-已撤销, 5-已退款, 6-订单关闭）")
    @TableField("state")
    private Integer state;

    @ApiModelProperty("支付方式（微信，支付宝）")
    @TableField("pay_method")
    private String payMethod;

    @ApiModelProperty("创建时间")
    @TableField(value = "created_time", fill = FieldFill.INSERT)
    private Date createdTime;

    @ApiModelProperty("更新时间")
    @TableField(value = "updated_time", fill = FieldFill.INSERT_UPDATE)
    private Date updatedTime;

    @ApiModelProperty("逻辑删除")
    @TableField("deleted")
    @TableLogic
    private Integer deleted;

    /**
     * 基于自动注入的构造
     *
     * @param outTradeNo 商户订单号
     * @param amount 支付金额
     * @param state 订单状态
     * @param payMethod 支付方式
     */
    public Order(Long outTradeNo, String amount, Integer state, String payMethod) {
        this.outTradeNo = outTradeNo;
        this.amount = amount;
        this.state = state;
        this.payMethod = payMethod;
    }
}
