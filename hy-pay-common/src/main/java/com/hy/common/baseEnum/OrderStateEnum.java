package com.hy.common.baseEnum;

/**
 * @作用 订单状态枚举
 * @作者 坏银
 * @时间 2022/4/19 16:21
 */
public class OrderStateEnum {

    public static int 订单生成() {
        return 0;
    }

    public static int 订单支付中() {
        return 1;
    }

    public static int 订单支付成功() {
        return 2;
    }

    public static int 订单已撤销() {
        return 3;
    }

    public static int 订单已退款() {
        return 5;
    }

    public static int 订单关闭() {
        return 6;
    }


}
