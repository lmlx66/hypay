package com.hy.common.exception;

import com.hy.common.Result.ResponseEnum;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @作用 自定义统一返回错误异常
 * @作者 坏银
 * @时间 2022/4/17 11:51
 */
@Data
@AllArgsConstructor
public class ExceptionResult {
    // 异常状态码
    private Integer code;

    // 异常的名字,也可以写一些dbug消息
    private String exception;

    // 返回用户的异常消息
    private String message;

    /**
     * 常规静态构造
     *
     * @return ExceptionResult
     * @Param code
     * @Param exception
     * @Param message
     */
    public static ExceptionResult fail(Integer code, String exception, String message) {
        return new ExceptionResult(code, exception, message);
    }


    /**
     * 枚举及异常类名构造
     *
     * @param responseEnum
     * @param exception
     * @return exceptionResult
     */
    public static ExceptionResult fail(ResponseEnum responseEnum, Exception exception) {
        return fail(responseEnum.getCode(), exception.getClass().getName(), responseEnum.getMessage());
    }

    /**
     * 枚举构造
     *
     * @param responseEnum
     * @return exceptionResult
     */
    public static ExceptionResult fail(ResponseEnum responseEnum) {
        return fail(responseEnum.getCode(), null, responseEnum.getMessage());
    }
}
