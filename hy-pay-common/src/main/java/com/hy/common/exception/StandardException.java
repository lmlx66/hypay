package com.hy.common.exception;

import com.hy.common.Result.ResponseEnum;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @作用 自定义的常规异常
 * @作者 坏银
 * @时间 2022/4/17 12:11
 */

@Data
@AllArgsConstructor
public class StandardException extends RuntimeException {
    //错误代码
    private Integer code;
    //错误消息
    private String message;

    /**
     * 传入用户错误枚举类型
     *
     * @param responseEnum 例如： USER_REG_USER_PASSWORD_CODE(401, "用户名和密码错误"),
     */
    public StandardException(ResponseEnum responseEnum) {
        this.code = responseEnum.getCode();
        this.message = responseEnum.getMessage();
    }

}
