package com.hy.utils;

import cn.hutool.extra.qrcode.QrCodeUtil;

import java.awt.image.BufferedImage;

/**
 * @作用
 * @作者 坏银
 * @时间 2022/4/13 16:09
 */
public class QrCodeUtils {
    /**
     * 生成普通二维码，默认宽高200
     *
     * @param url 链接地址
     * @return 图片
     */
    public static BufferedImage generate(String url) {
        return QrCodeUtil.generate(url, 200, 200);
    }

    /**
     * 根据长宽生成二维码
     *
     * @param url    链接地址
     * @param width  宽度
     * @param height 高度
     * @return 图片
     */
    public static BufferedImage generate(String url, int width, int height) {
        return QrCodeUtil.generate(url, width, height);
    }
}
