package com.hy.utils.aliResponse;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AliResponseEnum {
    /**
     * 交易查询响应key
     */
    TRADE_QUERY_RESPONSE("alipay_trade_query_response"),

    /**
     * 当面付预创建响应key
     */
    TRADE_PRECREATE_RESPONSE("alipay_trade_precreate_response"),

    /**
     * 创建订单响应key
     */
    TRADE_CREATE_RESPONSE("alipay_trade_create_response"),

    /**
     * 商户余额查询
     */
    FUND_ACCOUNT_QUERY_RESPONSE("alipay_fund_account_query_response"),
    ;

    // 二级响应体key
    private final String key;
}
