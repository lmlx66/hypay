package com.hy.utils.aliResponse;

import com.hy.common.Result.ResponseEnum;
import com.hy.common.baseEnum.OrderStateEnum;
import com.hy.common.exception.StandardException;

/**
 * @author: 王富贵
 * @description: 支付宝订单状态工具
 * @createTime: 2022年04月21日 14:48:51
 */
public class AliTradeStatusUtils {
    /**
     * 将订单状态转换为中文
     *
     * @param state 英文订单状态
     * @return String 英文订单状态
     */
    public static String getTradeStatusWithChinese(String state) {
        if ("WAIT_BUYER_PAY".equals(state)) {
            return "交易创建，等待买家付款";
        }
        if ("TRADE_CLOSED".equals(state)) {
            return "未付款交易超时关闭，或支付完成后全额退款";
        }
        if ("TRADE_SUCCESS".equals(state)) {
            return "交易支付成功";
        }
        if ("TRADE_FINISHED".equals(state)) {
            return "交易结束，不可退款";
        }
        //都不是表示参数为空或不合法
        throw new StandardException(ResponseEnum.A_LI_REQUEST_PARAMETER_IS_EMPTY_OR_DOES_NOT_EXIST);
    }

    /**
     * 根据响应订单状态返回订单状态码
     *
     * @param state 响应订单状态
     * @return 订单状态码
     */
    public static Integer getTradeStatusCode(String state) {
        if ("WAIT_BUYER_PAY".equals(state)) {
            return OrderStateEnum.订单支付中();
        }

        if ("TRADE_CLOSED".equals(state)) {
            return OrderStateEnum.订单已撤销();
        }
        if ("TRADE_SUCCESS".equals(state)) {
            return OrderStateEnum.订单支付成功();
        }
        if ("TRADE_FINISHED".equals(state)) {
            return OrderStateEnum.订单关闭();
        }

        //都不是表示参数为空或不合法
        throw new StandardException(ResponseEnum.A_LI_REQUEST_PARAMETER_IS_EMPTY_OR_DOES_NOT_EXIST);
    }
}
