package com.hy.utils.aliResponse;

import com.alibaba.fastjson.JSONObject;
import com.hy.common.Result.Result;

import java.util.Map;

/**
 * @作用 阿里官方响应封装
 * @作者 坏银
 * @时间 2022/4/18 10:59
 */
public class AliResponseUtils {

    /**
     * 二级响应体判断请求是否成功
     *
     * @param responseBody 二级响应体内容
     * @return boolean true 成功 | false 失败
     */
    public static boolean judgeSuccess(Map responseBody) {
        String code = (String) responseBody.get("code");
        if ("10000".equals(code)) {
            return true;
        }
        return false;
    }

    /**
     * 根据总响应判断请求是否成功
     *
     * @param JsonResponse 总响应头内容
     * @param key          二级响应体key
     * @return
     */
    public static boolean judgeSuccess(String JsonResponse, String key) {
        Map map = getResponseBody(JsonResponse, key);
        String code = (String) map.get("code");
        if ("10000".equals(code)) {
            return true;
        }
        return false;
    }

    /**
     * 通过key获取阿里响应的二级内容
     *
     * @param JsonResponse 响应体
     * @param key          内容key
     * @return Map 二级响应内容主体
     */
    public static Map getResponseBody(String JsonResponse, String key) {
        return JSONObject.parseObject(JsonResponse).getJSONObject(key);
    }


    /**
     * 根据二级响应体获取code码和消息
     *
     * @param responseBody 二级响应体
     * @return HashMap<Object, Object> code码和消息
     */
    public static Result getResponseBodyCodeAndMessage(Map responseBody) {
        String code = (String) responseBody.get("code");
        String sub_msg = (String) responseBody.get("sub_msg");
        return Result.error(Integer.valueOf(code), sub_msg);
    }

    /**
     * 根据响应体获取二级响应体的code码和消息
     *
     * @param JsonResponse 响应体
     * @param key          内容key
     * @return HashMap<Object, Object> code码和消息
     */
    public static Result getResponseBodyCodeAndMessage(String JsonResponse, String key) {
        Map responseBody = getResponseBody(JsonResponse, key);
        return getResponseBodyCodeAndMessage(responseBody);
    }

    /**
     * 异步回调接口支付成功获取商户订单号
     *
     * @param response 回执响应内容
     * @return String 商户订单号
     * <p>
     * 该接口的参数演示如下
     * {
     * "gmt_create": "2022-04-19 17:57:25",
     * "charset": "UTF-8",
     * "seller_email": "etnmui0812@sandbox.com",
     * "subject": "hypay支付宝扫码支付测试",
     * "sign": "签名",
     * "buyer_id": "2088622958363125",
     * "invoice_amount": "86.00",
     * "notify_id": "2022041900222175734063120531067401",
     * "fund_bill_list": "[{\"amount\":\"86.00\",\"fundChannel\":\"ALIPAYACCOUNT\"}]",
     * "notify_type": "trade_status_sync",
     * "trade_status": "TRADE_SUCCESS",
     * "receipt_amount": "86.00",
     * "buyer_pay_amount": "86.00",
     * "app_id": "2021000119665039",
     * "sign_type": "RSA2",
     * "seller_id": "2088621958345111",
     * "gmt_payment": "2022-04-19 17:57:33",
     * "notify_time": "2022-04-19 17:57:34",
     * "version": "1.0",
     * "out_trade_no": "38485316558917",
     * "total_amount": "86.00",
     * "trade_no": "2022041922001463120502988907",
     * "auth_app_id": "2021000119665039",
     * "buyer_logon_id": "enl***@sandbox.com",
     * "point_amount": "0.00"
     * }
     */
    public static String getNotifyOutTradeNo(Map<String, String> response) {
        //商户订单号
        return response.get("out_trade_no");
    }
}
