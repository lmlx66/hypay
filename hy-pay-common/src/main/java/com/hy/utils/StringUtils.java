package com.hy.utils;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * @作用 字符工具类，继承lang3
 * @作者 坏银
 * @时间 2022/4/13 17:15
 */
public final class StringUtils extends org.apache.commons.lang3.StringUtils {

    public static String encode(String str) {
        String encode;
        encode = URLEncoder.encode(str, StandardCharsets.UTF_8);
        return encode;
    }

}