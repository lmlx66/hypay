package com.hy.utils;

import cn.hutool.http.HttpRequest;

/**
 * @作用 http请求工具
 * @作者 坏银
 * @时间 2022/4/13 17:15
 */
public class HttpUtils {
    public static String HttpGetRequestByJson(String url) {
        return HttpRequest.get(url).execute().body();
    }
}
