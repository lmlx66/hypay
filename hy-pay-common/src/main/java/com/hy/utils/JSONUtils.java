package com.hy.utils;

import com.alibaba.fastjson.JSONObject;

import java.util.Map;

public class JSONUtils {
    /**
     * 获取json中的二级键值对
     *
     * @param jsonStr json字符串
     * @param key 二级键值对key
     * @return JSONObject
     */
    public static JSONObject getSecondaryObj(String jsonStr, String key) {
        return JSONObject.parseObject(jsonStr).getJSONObject("key");
    }

    /**
     * 获取json中的二级键值对
     *
     * @param jsonStr json字符串
     * @param key 二级键值对key
     * @return map
     */
    public static Map getSecondaryMap(String jsonStr, String key) {
        return JSONObject.parseObject(jsonStr).getJSONObject("key");
    }
}
