package com.hy.utils;

import cn.hutool.core.codec.Base64;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * @作用 base64工具
 * @作者 坏银
 * @时间 2022/4/17 13:46
 */
public class Base64Utils {
    /**
     * 将BufferedImage转png base64格式
     * @param bufferedImage
     * @return String png图片的base64字符串
     */
    public static String GetBase64Image(BufferedImage bufferedImage) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ImageIO.write(bufferedImage,"png",byteArrayOutputStream);
        String encode = Base64.encode(byteArrayOutputStream.toByteArray());
        return "data:image/png;base64," + encode;
    }
}
