package com.hy.utils;

import cn.hutool.core.date.DateUtil;

import java.util.Date;

/**
 * @author: 王富贵
 * @description: 日期工具类
 * @createTime: 2022年04月21日 19:28:30
 */
public class DateUtils {
    /**
     * 将时间戳转化为 年-月-日 时:分:秒
     * @param date 时间戳
     * @return 年-月-日 时:分:秒
     */
    public static String convertDate(Date date) {
        return DateUtil.format(date, "yyyy.MM.dd HH:mm:ss");
    }
}
