package com.hy.utils;

import com.hy.common.Result.ResponseEnum;
import com.hy.common.exception.StandardException;

/**
 * @作用 订单工具类
 * @作者 坏银
 * @时间 2022/4/20 9:46
 */
public class OrderUtils {
    /**
     * 判断当前订单状态
     *
     * @param state 状态参数
     * @return true 成功 | false 失败
     */
    public static String getOrderState(Integer state) {
        if (state == 0) {
            return "订单生成";
        }
        if (state == 1) {
            return "订单支付中";
        }
        if (state == 2) {
            return "订单支付成功";
        }
        if (state == 3) {
            return "订单已撤销";
        }
        if (state == 4) {
            return "订单已退款";
        }
        if (state == 5) {
            return "订单关闭";
        }
        //订单状态不存在
        throw new StandardException(ResponseEnum.ORDER_STATUS_IS_INVALID);
    }
}
