## 📚 项目介绍

[hypay](https://gitee.com/lmlx66/hypay) 是一套适合互联网使用的开源支付系统，目前仅支持单服务商或普通商户模式使用。对接支付宝官方接口。

hypay使用`spring boot`和`Element UI Vue`开发，是一套非常实用的web开发框架

感谢：[**廖永轩轩轩**](https://gitee.com/ganlai)的前端支持



## 🍎 项目进度与规划

当前进度

* 对接`支付宝`服务商和普通商户接口，支持RSA和RSA2签名以及证书
* 提供http形式接口，方便对接
* 接口请求和响应数据采用签名机制，保证交易安全可靠
* 支付平台到商户系统订单通知使用MQ实现，保证了高可用，消息可达
* 使用前后端分离架构，方便二次开发
* 雪花漂移算法支持多机分布式id



规划

* 支持多渠道对接，支付网关自动路由
* 对接`微信`服务商和普通商户接口，支持`V2`和`V3`接口
* 已对接`云闪付`服务商接口，可选择多家支付机构
* 系统安全，支持`分布式`部署，`高并发`
* 集成Redis做缓存
* 采用`satoken`实现权限管理



## 🥞 系统架构

> hypay支付系统架构图

![输入图片说明](doc/pic/2.png)




> 核心技术栈

| 软件名称                                          | 描述                              | 版本                |
| ------------------------------------------------- | --------------------------------- | ------------------- |
| Jdk                                               | Java环境                          | 11                  |
| Spring Boot                                       | 开发框架                          | 2.4.8               |
| Redis                                             | 分布式缓存                        | 3.2.8 或 高版本     |
| MySQL                                             | 数据库                            | 5.7.X 或 8.0 高版本 |
| MQ                                                | 消息中间件                        | RocketMQ            |
| yitter-idgenerator                                | 雪花算法生成器                    | 1.0.6               |
| [ElementUI Vue](https://element.eleme.io/#/zh-CN) | Ant Design的Vue实现，前端开发使用 | v2 laster           |
| [MyBatis-Plus](https://mp.baomidou.com/)          | MyBatis增强工具                   | 3.4.1               |
| [Hutool](https://www.hutool.cn/)                  | Java工具类库                      | 5.7.22              |
| [knife4j](https://doc.xiaominfo.com/)             | swagger增强工具                   | 2.0.9               |
| [IJPay](https://gitee.com/javen205/IJPay)         | 支付开发工具                      | 2.8.0               |



> 项目结构

```lua
hypay-ui  -- https://gitee.com/ganlai/hypay-ui

hypay
├── doc
	├── hypay.sql -- 初始化sql文件
└── hypay-common -- 公共组件目录
├── hypay-generator -- mybatis-pluis代码生成器
├── hypay-web-demo -- hypay支付后端代码
	├── hypay-service -- hypay支付服务[8080]
	└── hypay-rocketmq -- hypay支付消息rocketmq接收端demo[8091]
```



## 🍠项目截图

支付页面

![输入图片说明](doc/pic/3.png)



订单页面

![输入图片说明](doc/pic/4.png)



动态接口文档页面

![输入图片说明](doc/pic/5.png)



## 🍿 功能模块

[接口文档地址](https://www.apifox.cn/apidoc/shared-69e70192-9aa0-480c-a20c-2cb5f194c30d)
### 1、订单模板



### 2、支付宝接口模块



### 3、ID生成模块



## 🎁启动项目

1. 导入sql文件
2. 配置数据库信息
3. 配置rocketmq信息
4. 配置`resources/production/alipay.properties`对应的签名或者证书。或者使用默认的沙箱配置
5. 配置`resources/production/alipay.properties`下的回调地址
6. 启动hypay-service
7. 启动hypay-rocketmq-demo



### 1、测试沙箱账号

| 商家信息 |                  |
| -------- | ---------------- |
| 商家账号 | etnmui0812@sandbox.com                 |
| 登录密码 | 111111           |
| 商户PID  | 2088621958345111 |



| 买家信息 |                        |
| -------- | ---------------------- |
| 买家账号 | enltsv4803@sandbox.com |
| 登录密码 | 111111                 |
| 支付密码 | 111111                 |
| 用户UID  | 2088622958363125       |
| 用户名称 | enltsv4803             |
| 证件类型 | 身份证(IDENTITY_CARD)  |
| 证件号码 | 022187197411099762     |

## 🍗最佳实践

1. 请务必校验签名，并使用https保护支付安全
2. 由于是支付相关的程序。需要保证高安全与可靠性。rocketmq需要保证消息可靠性，消息安全性，消息幂等性

## 🍬技术亮点

* 使用io密集型线程池技术，异步发送支付成功rocketmq消息，异步同步数据库。
* 高效的使用Lambda、parallelStream并行流和Optional判空等jdk8新特性。
* 采取新式漂移雪花算法生成id，保证高并发下的id生成不成为瓶颈。
  * 同时也可以自定义机器码长度、序列数长度。
  * 本算法在保持并发性能（5W+/0.01s）和最大64个 WorkerId（6bit）的同时，能用70年才到 js Number Max 值。避免前端传播过程还需要转String类型
* 支持Rocketmq消息队列的广播模式，异步发送支付消息。将支付成功后续处理逻辑解耦。
* 自动捕获异常，自动装箱Result类返回

## 😭小遗憾

1. 由于个人单打独斗，包括对接口文档维护，前端设计并没有成员能够完成，因此系统并没有开发完成。
2. 采用redis缓存，缓存一致采用双写双删或canal组件。未实现。
3. 对账相关的功能未实现
