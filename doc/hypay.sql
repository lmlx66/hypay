/*
 Navicat Premium Data Transfer

 Source Server         : lx
 Source Server Type    : MySQL
 Source Server Version : 50732
 Source Host           : localhost:3306
 Source Schema         : hypay

 Target Server Type    : MySQL
 Target Server Version : 50732
 File Encoding         : 65001

 Date: 22/04/2022 19:09:01
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for pay_order
-- ----------------------------
DROP TABLE IF EXISTS `pay_order`;
CREATE TABLE `pay_order`  (
  `out_trade_no` bigint(64) NOT NULL COMMENT '商户订单号',
  `amount` bigint(100) NOT NULL COMMENT '支付金额',
  `state` int(10) NOT NULL COMMENT '订单支付状态：（ 0-订单生成, 1-支付中, 2-支付成功, 3-支付失败, 4-已撤销, 5-已退款, 6-订单关闭）',
  `pay_method` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '支付方式（微信，支付宝）',
  `created_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `deleted` int(1) NULL DEFAULT 0 COMMENT '逻辑删除'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of pay_order
-- ----------------------------
INSERT INTO `pay_order` VALUES (39500516847685, 89, 1, '支付宝', '2022-04-22 14:48:01', '2022-04-22 14:48:01', 0);
INSERT INTO `pay_order` VALUES (39501529104453, 89, 3, '支付宝', '2022-04-22 14:52:08', '2022-04-22 14:52:08', 0);
INSERT INTO `pay_order` VALUES (39503137280069, 86, 1, '支付宝', '2022-04-22 14:58:41', '2022-04-22 14:58:41', 0);
INSERT INTO `pay_order` VALUES (39503207944261, 89, 2, '支付宝', '2022-04-22 14:58:58', '2022-04-22 14:58:58', 0);
INSERT INTO `pay_order` VALUES (39503262285893, 89, 1, '支付宝', '2022-04-22 14:59:11', '2022-04-22 14:59:11', 0);
INSERT INTO `pay_order` VALUES (39503339032645, 89, 2, '支付宝', '2022-04-22 14:59:30', '2022-04-22 14:59:30', 0);
INSERT INTO `pay_order` VALUES (39503363981381, 86, 2, '支付宝', '2022-04-22 14:59:36', '2022-04-22 14:59:36', 0);
INSERT INTO `pay_order` VALUES (39504862478405, 89, 3, '支付宝', '2022-04-22 15:05:42', '2022-04-22 15:05:42', 0);
INSERT INTO `pay_order` VALUES (39508339560517, 89, 2, '支付宝', '2022-04-22 15:19:51', '2022-04-22 15:19:51', 0);
INSERT INTO `pay_order` VALUES (39510346829893, 89, 1, '支付宝', '2022-04-22 15:28:01', '2022-04-22 15:28:01', 0);
INSERT INTO `pay_order` VALUES (39510648774725, 89, 1, '支付宝', '2022-04-22 15:29:14', '2022-04-22 15:29:14', 0);
INSERT INTO `pay_order` VALUES (39510846431301, 89, 4, '支付宝', '2022-04-22 15:30:03', '2022-04-22 15:30:03', 0);
INSERT INTO `pay_order` VALUES (39510895902789, 89, 1, '支付宝', '2022-04-22 15:30:15', '2022-04-22 15:30:15', 0);
INSERT INTO `pay_order` VALUES (39511034519621, 89, 2, '支付宝', '2022-04-22 15:30:49', '2022-04-22 15:30:49', 0);
INSERT INTO `pay_order` VALUES (39511244439621, 89, 4, '支付宝', '2022-04-22 15:31:40', '2022-04-22 15:31:40', 0);
INSERT INTO `pay_order` VALUES (39511458943045, 89, 5, '支付宝', '2022-04-22 15:32:32', '2022-04-22 15:32:32', 0);
INSERT INTO `pay_order` VALUES (39519834206277, 89, 4, '支付宝', '2022-04-22 16:06:37', '2022-04-22 16:06:37', 0);
INSERT INTO `pay_order` VALUES (39522456920133, 89, 3, '支付宝', '2022-04-22 16:17:17', '2022-04-22 16:17:17', 0);
INSERT INTO `pay_order` VALUES (39526218920005, 86, 5, '支付宝', '2022-04-22 16:32:37', '2022-04-22 16:33:18', 0);
INSERT INTO `pay_order` VALUES (39557768405061, 89, 4, '支付宝', '2022-04-22 18:40:58', '2022-04-22 18:40:58', 0);
INSERT INTO `pay_order` VALUES (39558880895045, 89, 3, '支付宝', '2022-04-22 18:45:30', '2022-04-22 18:45:30', 0);
INSERT INTO `pay_order` VALUES (39559789363269, 89, 2, '支付宝', '2022-04-22 18:49:12', '2022-04-22 18:49:12', 0);
INSERT INTO `pay_order` VALUES (39560051339333, 89, 1, '支付宝', '2022-04-22 18:50:16', '2022-04-22 18:50:16', 0);
INSERT INTO `pay_order` VALUES (39560221536325, 89, 0, '支付宝', '2022-04-22 18:50:57', '2022-04-22 18:50:57', 0);
INSERT INTO `pay_order` VALUES (39562192048197, 89, 1, '支付宝', '2022-04-22 18:58:58', '2022-04-22 18:58:58', 0);

SET FOREIGN_KEY_CHECKS = 1;
