package com.hy.common.rocketmq;

import org.apache.rocketmq.spring.annotation.MessageModel;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

import java.util.HashMap;

/**
 * 对发送成功的模组采用广播模式处理
 */
@Component
@RocketMQMessageListener(consumerGroup = "hypay-consumer", topic = "pay-success-topic", messageModel = MessageModel.BROADCASTING)
public class PaySuccess implements RocketMQListener<HashMap<String, Long>> {

    @Override
    public void onMessage(HashMap<String, Long> map) {
        System.out.println("订单:" + map.get("out_trade_no") + " 支付成功");
        //TODO 支付成功的后续操作
    }
}
