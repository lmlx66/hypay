package com.hy;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.hy.common.entity.Order;
import com.hy.mapper.OrderMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @作用 mybatis-plus测试
 * @作者 坏银
 * @时间 2022/4/19 16:51
 */
@SpringBootTest
public class MybatisPlusTest {

    @Autowired
    private OrderMapper orderMapper;

    @Test
    public void testOrder(){
        Long outTradeNo = Long.valueOf("38468807192645");
        orderMapper.delete(new LambdaQueryWrapper<Order>().eq(Order::getOutTradeNo,outTradeNo));
    }

}
