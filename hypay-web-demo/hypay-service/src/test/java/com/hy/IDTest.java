package com.hy;

import cn.hutool.core.date.DateUtil;
import com.hy.common.Utils.IdGenerate;
import com.hy.corecode.idgen.WFGIdGenerator;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * @作用 1640966400000
 * @作者 坏银
 * @时间 2022/4/13 17:44
 */
@SpringBootTest
public class IDTest {

    @Resource
    private WFGIdGenerator wfgIdGenerator;
    @Test
    public void getId(){
        long l =wfgIdGenerator.next();
        System.out.println(l);
        System.out.println(DateUtil.parse("2022/1/1").getTime());
    }
}
