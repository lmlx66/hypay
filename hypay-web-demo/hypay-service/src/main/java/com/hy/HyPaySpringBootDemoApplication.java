package com.hy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @作用 主启动类
 * @作者 坏银
 * @时间 2022/4/13 16:55
 */
@SpringBootApplication
public class HyPaySpringBootDemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(HyPaySpringBootDemoApplication.class, args);
    }
}
