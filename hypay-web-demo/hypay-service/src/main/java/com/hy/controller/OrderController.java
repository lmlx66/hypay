package com.hy.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hy.common.vo.OrderVo;
import com.hy.common.vo.PageVo;
import com.hy.service.OrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * @作用 支付订单相关接口
 * @作者 坏银
 * @时间 2022/4/20 9:46
 */
@RestController
@RequestMapping("api/order")
@Api(tags = "支付订单控制层")
public class OrderController {

    @Resource
    private OrderService orderService;

    @ApiOperation(value = "查询订单支付状态", notes = "根据商户订单号查询商户订单状态")
    @ApiImplicitParam(name = "outTradeNo", value = "商户订单号", required = true)
    @ResponseBody
    @GetMapping("getOrderState")
    public Map<Object, Object> getOrderState(@RequestParam(value = "outTradeNo") Long outTradeNo) {
        String orderState = orderService.getOrderState(outTradeNo);
        Map<Object, Object> resultMap = new HashMap<>();
        resultMap.put("支付状态", orderState);
        return resultMap;
    }

    @ApiOperation(value = "分页查询订单", notes = "根据当前页和页面大小查询订单集合")
    @ResponseBody
    @PostMapping("getPageOrderList")
    public Page<OrderVo> getPageOrderList(@RequestBody PageVo pageVo) {
        return orderService.getPageOrderList(pageVo.getCurrent(), pageVo.getSize());
    }
}
