package com.hy.controller;

import com.hy.corecode.idgen.WFGIdGenerator;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;

/**
 * @作用 获取一个id
 * @作者 坏银
 * @时间 2022/4/13 17:55
 */
@Api(tags = "ID控制层")
@RestController
@RequestMapping("api")
public class GenerateId {
    @Resource
    private WFGIdGenerator wfgIdGenerator;

    @GetMapping(value = "GetId")
    @ApiOperation(value = "获取一个订单id")
    public HashMap<String, Long> getId() {
        HashMap<String, Long> map = new HashMap<>();
        map.put("id", wfgIdGenerator.next());
        return map;
    }
}
