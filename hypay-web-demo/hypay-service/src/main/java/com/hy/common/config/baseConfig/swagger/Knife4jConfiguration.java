package com.hy.common.config.baseConfig.swagger;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

/**
 * @作用 swagger配置
 * @作者 坏银
 * @时间 2022/4/14 11:40
 */
@Configuration
@EnableSwagger2WebMvc
public class Knife4jConfiguration {

    //标题
    private static final String title = "hypay支付系统demo";
    //描述
    private static final String description = "hypay支付系统demo";

    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.hy.controller"))//包扫描
                .paths(PathSelectors.any())//路径
                .build()
                .groupName("王富贵")
                .enable(true);
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(title)
                .description(description)
                .contact(new Contact("王富贵", "www.lmlx66.top", "lmlm1938857445@163.com"))
                .version("1.0")
                .build();
    }

}