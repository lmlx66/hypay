package com.hy.common.interceptor;


import com.alipay.api.AlipayApiException;
import com.hy.common.Result.ResponseEnum;
import com.hy.common.exception.StandardException;
import com.hy.controller.ali.AbstractAliPayApiController;
import com.ijpay.alipay.AliPayApiConfigKit;
import net.bytebuddy.dynamic.loading.ByteArrayClassLoader;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 支付宝拦截器，注入支付宝相关配置信息
 */
public class AliPayInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object handler) throws AlipayApiException {
        if (HandlerMethod.class.equals(handler.getClass())) {
            HandlerMethod method = (HandlerMethod) handler;
            Object controller = method.getBean();
            if (!(controller instanceof AbstractAliPayApiController)) {
                throw new StandardException(ResponseEnum.A_LI_CONTROLLER_NEEDS_FOUNDATION);
            }
            AliPayApiConfigKit.setThreadLocalAliPayApiConfig(((AbstractAliPayApiController) controller).getApiConfig());
            return true;
        }
        return false;
    }

}