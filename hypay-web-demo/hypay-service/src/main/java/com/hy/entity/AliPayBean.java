package com.hy.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * @作用 阿里支付宝支付基本配置
 * @作者 坏银
 * @时间 2022/4/13 16:57
 */
@Data
@Component
@AllArgsConstructor
@NoArgsConstructor
@PropertySource("classpath:/production/alipay.properties")
@ConfigurationProperties(prefix = "alipay")
public class AliPayBean {
    private String appId;
    private String privateKey;
    private String publicKey;
    private String appCertPath;
    private String aliPayCertPath;
    private String aliPayRootCertPath;
    private String serverUrl;
    private String domain;
}
