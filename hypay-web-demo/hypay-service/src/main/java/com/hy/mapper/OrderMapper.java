package com.hy.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hy.common.entity.Order;
import org.apache.ibatis.annotations.Mapper;

/**
 * Mapper 接口
 *
 * @author 王富贵
 * @since 2022-04-19
 */
@Mapper
public interface OrderMapper extends BaseMapper<Order> {

}
