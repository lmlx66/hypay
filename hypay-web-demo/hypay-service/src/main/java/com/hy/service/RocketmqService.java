package com.hy.service;

/**
 * @author: 王富贵
 * @description: rocketmq消息队列服务层
 * @createTime: 2022年04月21日 21:34:54
 */
public interface RocketmqService {
    /**
     * 发送支付成功的消息
     *
     * @param outTradeNo 商户订单号
     */
    void paySuccess(Long outTradeNo);
}
