package com.hy.service.envent;

import com.hy.common.entity.Order;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

/**
 * @author: 王富贵
 * @description: 支付成功发送MQ事件
 * @createTime: 2022/11/09 17:43
 */
@Getter
@Setter
public class PaymentSuccessEvent extends ApplicationEvent {

    // 订单
    private Order order;

    public PaymentSuccessEvent(Order order) {
        super(order);
        this.order = order;
    }
}
