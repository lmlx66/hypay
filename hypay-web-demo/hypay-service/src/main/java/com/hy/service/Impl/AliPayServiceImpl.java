package com.hy.service.Impl;

import com.alipay.api.domain.*;
import com.hy.common.Result.ResponseEnum;
import com.hy.common.exception.StandardException;
import com.hy.corecode.idgen.WFGIdGenerator;
import com.hy.service.AliPayService;
import com.hy.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Optional;

/**
 * @author: 王富贵
 * @description: 阿里支付服务层实现类
 * @createTime: 2022年4月19日 20:36:09
 */
@Service
@Slf4j
public class AliPayServiceImpl implements AliPayService {
    @Resource
    private WFGIdGenerator wfgIdGenerator;

    /**
     * 构建当面付模型
     *
     * @param inOutTradeNo  商户订单号
     * @param inTotalAmount 支付金额
     * @return 当面付模型
     */
    @Override
    public AlipayTradePrecreateModel constructTradePreCreatePay(Long inOutTradeNo, String inTotalAmount) {
        /**
         * 1.订单基本信息配置
         */
        //订单详情
        String subject = "hypay支付宝扫码支付测试";
        //默认订单超时时间
        String timeoutExpress = "5m";
        //支付金额，如果没输入就是默认86元
        String totalAmount = Optional
                .ofNullable(inTotalAmount)
                .orElse("86");
        // 订单id，参数为空，生成一个;参数不为空，用传入参数
        Long outTradeNo = Optional
                .ofNullable(inOutTradeNo)
                .orElse(wfgIdGenerator.next());

        /**
         * 2.配置订单模型
         */
        AlipayTradePrecreateModel model = new AlipayTradePrecreateModel();
        model.setSubject(subject);
        model.setTimeoutExpress(timeoutExpress);
        model.setTotalAmount(totalAmount);
        model.setOutTradeNo(String.valueOf(outTradeNo));

        return model;
    }

    /**
     * 构建pc浏览器支付模型
     *
     * @param inOutTradeNo  商户订单号
     * @param inTotalAmount 支付金额
     * @return pc浏览器支付模型
     */
    @Override
    public AlipayTradePagePayModel constructTradePagePayModel(Long inOutTradeNo, String inTotalAmount) {
        /**
         * 1.订单基本信息
         */
        //订单详情
        String subject = "hypay PC支付测试";
        //商户订单号
        Long outTradeNo = Optional
                .ofNullable(inOutTradeNo)
                .orElse(wfgIdGenerator.next());
        //总金额
        String totalAmount = Optional
                .ofNullable(inTotalAmount)
                .orElse("88.88");
        //回传参数
        String passBackParams = "pass-back_params";


        /**
         * 配置订单模型
         */
        AlipayTradePagePayModel model = new AlipayTradePagePayModel();
        model.setProductCode("FAST_INSTANT_TRADE_PAY");
        model.setOutTradeNo(String.valueOf(outTradeNo));
        model.setTotalAmount(totalAmount);
        model.setSubject(subject);
        model.setPassbackParams(passBackParams);

        /**
         * 花呗分期相关的设置,测试环境不支持花呗分期的测试
         * hb_fq_num代表花呗分期数，仅支持传入3、6、12，其他期数暂不支持，传入会报错；
         * hb_fq_seller_percent代表卖家承担收费比例，商家承担手续费传入100，用户承担手续费传入0，仅支持传入100、0两种，其他比例暂不支持，传入会报错。
         */
            /*
            ExtendParams extendParams = new ExtendParams();
            extendParams.setHbFqNum("3");
            extendParams.setHbFqSellerPercent("0");
            model.setExtendParams(extendParams);
             */
        return model;
    }

    /**
     * 构建创建订单模型
     *
     * @param inOutTradeNo  商户订单号
     * @param inTotalAmount 支付金额
     * @return 创建支付宝订单模型
     */
    @Override
    public AlipayTradeCreateModel constructTradeCreateModel(Long inOutTradeNo, String inTotalAmount) {

        // 订单id，参数为空，生成一个;参数不为空，不生成
        Long notNullId = Optional
                .ofNullable(inOutTradeNo)
                .orElse(wfgIdGenerator.next());
        //支付金额
        String totalAmount = Optional
                .ofNullable(inTotalAmount)
                .orElse("88.88");

        //构建创建订单模型
        AlipayTradeCreateModel model = new AlipayTradeCreateModel();
        model.setOutTradeNo(String.valueOf(notNullId));
        model.setTotalAmount(totalAmount);
        model.setBody("Body");
        model.setSubject("hypay 测试统一收单交易创建接口");
        //TODO 支付宝id，和买家支付账号不能同时为空
        model.setBuyerId("2088622958363125");
        return model;
    }

    /**
     * 构建订单撤销模型
     *
     * @param outTradeNo 商户订单号
     * @param tradeNo    支付宝订单号
     * @return AlipayTradeCancelModel 订单撤销模型
     */
    @Override
    public AlipayTradeCancelModel constructTradeCancelModel(String outTradeNo, String tradeNo) {
        AlipayTradeCancelModel model = new AlipayTradeCancelModel();

        if (StringUtils.isEmpty(outTradeNo) && StringUtils.isEmpty(tradeNo)) {
            throw new StandardException(ResponseEnum.A_LI_REQUEST_PARAMETER_IS_EMPTY_OR_DOES_NOT_EXIST);
        }

        //优先使用商户订单号
        if (StringUtils.isNotEmpty(outTradeNo)) {
            model.setOutTradeNo(outTradeNo);
        }
        if (StringUtils.isNotEmpty(tradeNo)) {
            model.setTradeNo(tradeNo);
        }
        return model;
    }

    /**
     * 构建订单关闭模型
     *
     * @param outTradeNo 商户订单号
     * @param tradeNo    支付宝订单号
     * @return AlipayTradeCancelModel 订单关闭模型
     */
    @Override
    public AlipayTradeCloseModel constructTradeCloseModel(String outTradeNo, String tradeNo) {
        AlipayTradeCloseModel model = new AlipayTradeCloseModel();
        //优先使用商户订单号
        if (StringUtils.isNotEmpty(outTradeNo)) {
            model.setOutTradeNo(outTradeNo);
        }
        if (StringUtils.isNotEmpty(tradeNo)) {
            model.setTradeNo(tradeNo);
        }
        return model;
    }

    /**
     * 构建订单查询模型
     *
     * @param outTradeNo 商户订单号
     * @param tradeNo    支付宝订单号
     * @return AlipayTradeQueryModel 订单查询模型
     */
    @Override
    public AlipayTradeQueryModel constructTradeQueryModel(String outTradeNo, String tradeNo) {
        AlipayTradeQueryModel model = new AlipayTradeQueryModel();
        //优先使用商户订单号
        if (StringUtils.isNotEmpty(outTradeNo)) {
            model.setOutTradeNo(outTradeNo);
        }
        if (StringUtils.isNotEmpty(tradeNo)) {
            model.setTradeNo(tradeNo);
        }
        return model;
    }

    /**
     * 构建查询商户账号余额模型
     *
     * @param aliPayPId      商户pid
     * @param merchantUserId 商户账号
     * @return AlipayFundAccountQueryModel 商户账号余额查询
     */
    @Override
    public AlipayFundAccountQueryModel constructFundAccountQueryModel(String aliPayPId, String merchantUserId) {
        AlipayFundAccountQueryModel model = new AlipayFundAccountQueryModel();

        //查询的账号类型
        String accountType = "ACCTRANS_ACCOUNT";
        //商户pid或商户账号，优先使用商户账号
        if (StringUtils.isNotEmpty(aliPayPId)) {
            model.setAlipayUserId(aliPayPId);
        } else if (StringUtils.isNotEmpty(merchantUserId)) {
            //如果商户pid为空则改用商户账号
            model.setMerchantUserId(merchantUserId);
        } else {
            //抛出参数信息
            throw new StandardException(ResponseEnum.A_LI_REQUEST_PARAMETER_IS_EMPTY_OR_DOES_NOT_EXIST);
        }

        //参数准备
        model.setAccountType(accountType);
        return model;
    }
}
