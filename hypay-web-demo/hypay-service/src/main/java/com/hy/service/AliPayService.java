package com.hy.service;

import com.alipay.api.domain.*;

/**
 * @作用 阿里支付服务层
 * @作者 坏银
 * @时间 2022/4/19 20:35
 */
public interface AliPayService {
    /**
     * 构建当面付模型
     *
     * @param inOutTradeNo  商户订单号
     * @param inTotalAmount 支付金额
     * @return 当面付模型
     */
    AlipayTradePrecreateModel constructTradePreCreatePay(Long inOutTradeNo, String inTotalAmount);

    /**
     * 构建pc浏览器支付模型
     *
     * @param inOutTradeNo  商户订单号
     * @param inTotalAmount 支付金额
     * @return pc浏览器支付模型
     */
    AlipayTradePagePayModel constructTradePagePayModel(Long inOutTradeNo, String inTotalAmount);


    /**
     * 构建创建订单模型
     *
     * @param inOutTradeNo  商户订单号
     * @param inTotalAmount 支付金额
     * @return 创建支付宝订单模型
     */
    AlipayTradeCreateModel constructTradeCreateModel(Long inOutTradeNo, String inTotalAmount);

    /**
     * 构建订单撤销模型
     *
     * @param outTradeNo 商户订单号
     * @param tradeNo    支付宝订单号
     * @return AlipayTradeCancelModel 订单撤销模型
     */
    AlipayTradeCancelModel constructTradeCancelModel(String outTradeNo, String tradeNo);

    /**
     * 构建订单关闭模型
     *
     * @param outTradeNo 商户订单号
     * @param tradeNo    支付宝订单号
     * @return AlipayTradeCancelModel 订单关闭模型
     */
    AlipayTradeCloseModel constructTradeCloseModel(String outTradeNo, String tradeNo);

    /**
     * 构建订单查询模型
     *
     * @param outTradeNo 商户订单号
     * @param tradeNo    支付宝订单号
     * @return AlipayTradeQueryModel 订单查询模型
     */
    AlipayTradeQueryModel constructTradeQueryModel(String outTradeNo, String tradeNo);

    /**
     * 构建查询商户账号余额模型
     *
     * @param aliPayPId      商户pid
     * @param merchantUserId 商户账号
     * @return AlipayFundAccountQueryModel 商户账号余额查询
     */
    AlipayFundAccountQueryModel constructFundAccountQueryModel(String aliPayPId, String merchantUserId);
}
