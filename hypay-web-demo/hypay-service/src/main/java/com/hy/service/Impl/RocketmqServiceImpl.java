package com.hy.service.Impl;

import com.alibaba.fastjson.JSON;
import com.hy.service.RocketmqService;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;

/**
 * @author: 王富贵
 * @description: rocketmq消息队列服务层实现
 * @createTime: 2022年04月21日 21:38:09
 */
@Service
public class RocketmqServiceImpl implements RocketmqService {

    @Resource
    private RocketMQTemplate rocketMQTemplate;

    /**
     * 发送支付成功的消息
     * 异步请求
     *
     * @param outTradeNo 商户订单号
     */
    @Override
    public void paySuccess(Long outTradeNo) {
        HashMap<String, Long> result = new HashMap<>();
        result.put("out_trade_no", outTradeNo);
        rocketMQTemplate.convertAndSend("pay-success-topic", JSON.toJSONString(result).getBytes(StandardCharsets.UTF_8));
        //TODO 构建rocketmq消费者模块测试
    }
}
