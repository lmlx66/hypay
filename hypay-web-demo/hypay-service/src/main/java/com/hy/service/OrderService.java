package com.hy.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hy.common.entity.Order;
import com.hy.common.vo.OrderVo;

/**
 * 服务类
 *
 * @author 王富贵
 * @since 2022-04-19
 */
public interface OrderService extends IService<Order> {

    /**
     * 添加一个支付宝订单，默认为正在支付
     *
     * @param outTradeNo 订单号
     * @param amount     订单金额
     * @return true 插入成功 | false 插入失败
     */
    boolean putAliOrder(Long outTradeNo, String amount);

    /**
     * 订单状态修改为已支付
     *
     * @param outTradeNo 订单号
     */
    void orderPaid(Long outTradeNo);

    /**
     * 订单状态修改为已撤销
     *
     * @param outTradeNo 订单号
     */
    void orderCancel(Long outTradeNo);

    /**
     * 订单状态设置为已退款
     *
     * @param outTradeNo 订单号
     */
    void OrderRefunded(Long outTradeNo);

    /**
     * 订单状态设置为订单关闭
     *
     * @param outTradeNo 订单号
     */
    void orderClosed(Long outTradeNo);

    /**
     * 查询当前订单状态
     *
     * @param outTradeNo 订单号
     */
    String getOrderState(Long outTradeNo);

    /**
     * 获取订单分页数据
     * @param current 当前页数
     * @param size 页面大小
     * @return 订单集合
     */
    Page<OrderVo> getPageOrderList(Integer current, Integer size);

    /**
     * 数据库同步与远端订单状态
     *
     * @param tradeStatus 支付宝订单状态str
     * @param out_trade_no 订单号
     */
    void syncOrderStatus(String tradeStatus, String out_trade_no);
}
