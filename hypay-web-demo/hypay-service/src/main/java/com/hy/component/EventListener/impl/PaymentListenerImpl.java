package com.hy.component.EventListener.impl;

import com.hy.component.EventListener.PaymentListener;
import com.hy.service.RocketmqService;
import com.hy.service.envent.PaymentSuccessEvent;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author: 王富贵
 * @description: 支付成功消息发送监听实现
 * @createTime: 2022年08月04日 11:15:56
 */
@Service
public class PaymentListenerImpl implements PaymentListener {

    @Resource
    private RocketmqService rocketmqService;

    /**
     * 信息入库后，监听事务，事务提交成功后才进行rocketmq发送
     *
     * @param paymentSuccessEvent 支付事件实体
     */
    @Override
    public void paySuccess(PaymentSuccessEvent paymentSuccessEvent) {
        rocketmqService.paySuccess(paymentSuccessEvent.getOrder().getOutTradeNo());
    }
}
