package com.hy.component.EventListener;

import com.hy.service.envent.PaymentSuccessEvent;
import org.springframework.scheduling.annotation.Async;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

/**
 * @author: 王富贵
 * @description: 支付成功消息发送监听类
 * @createTime: 2022年08月04日 11:13:51
 */
public interface PaymentListener {
    /**
     * 信息入库后，监听事务，事务提交成功后才进行rocketmq发送
     *
     * @param paymentSuccessEvent 支付事件实体
     */
    // 使用异步池
    @Async("UpdateAndSendExecutor")
    // 开启事务监听
    @TransactionalEventListener(phase = TransactionPhase.BEFORE_COMMIT, classes = PaymentSuccessEvent.class)
    void paySuccess(PaymentSuccessEvent paymentSuccessEvent);
}
